import React, { Component } from 'react'
import TableData from './TableData';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table } from 'react-bootstrap';

export default class Home extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            books: [
              {
                id: 1,
                title: "មនុស្សពីរនាក់នៅផ្ទះជិតគ្នា",
                publishedYear: 2012,
                isHiding: false,
                action : 'Hide'
              },
              {
                id: 2,
                title: "គង់ហ៊ាន",
                publishedYear: 2015,
                isHiding: false,
                action : 'Hide'
              },
              {
                id: 3,
                title: "បុរសចេះថ្នាំពិសពស់",
                publishedYear: 2018,
                isHiding: false,
                action : 'Hide'
              },
              {
                id: 4,
                title: "អណ្ដើកនិងស្វា",
                publishedYear: 2019,
                isHiding: false,
                action : 'Hide'
              },
            ],
          };
      
    }
    
    render() {
        let data = this.state.books.map((td) => (
            // <TableData key={td.id} data={td} />  
            <TableData key = {td.id} data={td}/>
            ))
        return (
            
            <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Publish Year</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{data}</tbody>
      </Table>
        )
    }
}
