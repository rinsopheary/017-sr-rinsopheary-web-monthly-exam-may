import React, { Component } from 'react'

export default class TableData extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             id: this.props.data.id,
                title: this.props.data.title,
                publishedYear: this.props.data.publishedYear,
                isHiding: true,
                action : this.props.data.action
        };
    }
    handlerClick = () =>{

    
    }
    render() {
        return (
            <tr>
                <td>{this.state.id}</td>
                <td>{this.state.title}</td>
                <td>{this.state.publishedYear}</td>
                <td>
                    <button onClick ={this.handlerClick}>{this.state.action}</button>
                </td>
            </tr>
        )
    }
}
